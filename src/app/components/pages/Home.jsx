import React, {PropTypes} from 'react';
import Content from 'components/common/Content'

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let content= <Content />
    if(this.props.data) {
      content = <Content data={this.props.data.text}/>
    }
    return (
      <div>
        <h1>Home Page</h1>
        {content}
      </div>

  );
  }
}

Home.propTypes = {
};
