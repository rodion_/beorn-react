"use strict";

var React = require('react');
var _ = require('lodash')
var originalCreateElement = React.createElement;
var DivFactory = React.createFactory('div');

//function that works only with babel
function prototypeCompareIndex(array, item) {
  var result = -1;
  _.some(array, function (value, index) {
    let searchItem = "Object.getPrototypeOf\("+value;
    if (item.toString().indexOf(searchItem) > -1) {
      result = index;
      return true;
    }
  });
  return result;
}

module.exports = function(sandbox){
    return {
        stubChildren: function(stubbedComponents){
            sandbox.stub(React, 'createElement', function(component, props){
              var stubIndex = prototypeCompareIndex(stubbedComponents,component);
                if(stubIndex === -1){
                    return originalCreateElement.apply(React, arguments);
                }
                else {
                    let componentClassName = stubbedComponents[stubIndex];
                    if(props && props.className){
                        props.className = props.className + " " + componentClassName;
                    }
                    else if (props) {
                        props.className = componentClassName;
                    }

                    return DivFactory(props);
                }
            });
        }
    };

};
