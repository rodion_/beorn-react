import Route from '../src/app/routes/routes'
import Home from '../src/app/components/pages/home'
import Content from '../src/app/components/common/content'
var ReactSpecHelper = require("../src/test-utils/react-spec-helper");
var React = require('react/addons')
var Sinon = require('sinon')
var TestUtils = React.addons.TestUtils;

var home = TestUtils.renderIntoDocument(<Home />);

beforeEach(function() {
  this.sandbox = Sinon.sandbox.create();
  //Set up the spec helper.  Since React.createElement is static use sandbox.
  //Pass in the components you want to mock
  new ReactSpecHelper(this.sandbox).stubChildren(['content']);

});

afterEach(function() {
  this.sandbox.restore();
});

describe('Testing Home Page', () => {
  it('should display "Home page" as title', () => {
    let title = TestUtils.findRenderedDOMComponentWithTag(home, 'h1');
    expect(title.textContent).to.be.equal('Home Page')

  })
  it('should display a bloc content', () => {
    let content = TestUtils.scryRenderedComponentsWithType(home, Content);
    expect(content).to.exist;
  })

  it('should load text props within Content component', () => {
    let data = {text: "un text", other: "autre info"};
    let homeWithProps = TestUtils.renderIntoDocument(<Home  data={data}/>)
    let content = TestUtils.scryRenderedDOMComponentsWithClass(homeWithProps,'content');
    console.log("content : ", content);
    expect(content.length).to.be.equal(1)
    expect(content[0].props.data).to.be.equal("un text")
  })
})
